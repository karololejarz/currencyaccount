###Currency account application

####Building application

Uses Maven

####Connection to NBP API

Application connects to NBP API during start-up and persists the current A table for USD (not to call NBP api many times). As an example application there is no job for calling the api if the application runs uninterrupted for 2 days.

####Api documentation

http://localhost:8080/swagger-ui/

#####How to create an account

http://localhost:8080/swagger-ui/#/account-controller/openAccountWithStartingBalanceUsingPOST

#####How to view account's balance

http://localhost:8080/swagger-ui/#/account-controller/getAccountUsingGET

#####Operations on an account

2 example operations increasing the balance of the account:

1/ http://localhost:8080/swagger-ui/#/account-operation-controller/creditAccountWithPLNUsingPOST

2/ http://localhost:8080/swagger-ui/#/account-operation-controller/creditAccountWithUSDUsingPOST

2 more decreasing the balance of the account:

3/ http://localhost:8080/swagger-ui/#/account-operation-controller/payWithCardUSDUsingPOST

4/ http://localhost:8080/swagger-ui/#/account-operation-controller/withdrawUSDUsingPOST

#####Overview of operations on an account

http://localhost:8080/swagger-ui/#/operation-controller/getOperationsByAccountNumberUsingGET
