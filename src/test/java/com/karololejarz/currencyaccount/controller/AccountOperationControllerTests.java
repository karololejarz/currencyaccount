package com.karololejarz.currencyaccount.controller;

import com.google.gson.Gson;
import com.karololejarz.currencyaccount.dto.AccountCreateDto;
import com.karololejarz.currencyaccount.entity.Account;
import com.karololejarz.currencyaccount.entity.AccountType;
import com.karololejarz.currencyaccount.entity.Customer;
import com.karololejarz.currencyaccount.entity.Operation;
import com.karololejarz.currencyaccount.service.AccountService;
import com.karololejarz.currencyaccount.service.CreditOperationService;
import com.karololejarz.currencyaccount.service.OperationService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AccountOperationController.class)
public class AccountOperationControllerTests {
  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private CreditOperationService creditOperationService;
  @MockBean
  private AccountService accountService;
  @MockBean
  private OperationService operationService;

//  @Test
//  public void shouldCreditAccountWith10USD() throws Exception {
//    AccountCreateDto createDto = new AccountCreateDto("First", "Last", BigDecimal.TEN);
//    AccountType accountType = new AccountType(1L, "Rachunek Walutowy", "USD", null, null, null);
//    Customer owner = new Customer(createDto);
//    Account account = new Account(accountType, owner, BigDecimal.TEN);
//    account.setId(1L);
//
//    Operation operation = new Operation(account, "CREDITED_WITH_USD", BigDecimal.TEN);
//
//    when(accountService.openAccountWithStartingBalance(ArgumentMatchers.any(AccountCreateDto.class))).thenReturn(account);
//    when(operationService.saveOperation(operation)).thenReturn(operation);
//    when(creditOperationService.creditAccountWithUSD(1L, BigDecimal.TEN)).thenReturn(operation);
//
//    Gson gson = new Gson();
//    String jsonContent = gson.toJson(BigDecimal.TEN);
//
//    mockMvc.perform(post("/api/accounts/1/creditWithUSD")
//        .contentType(MediaType.APPLICATION_JSON)
//        .content(jsonContent)
//        .accept(MediaType.APPLICATION_JSON))
//        .andDo(print())
//        .andExpect(status().isOk())
//        .andExpect(jsonPath("$.operationValue", is(10)));
//  }
}
