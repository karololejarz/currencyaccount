package com.karololejarz.currencyaccount.controller;

import com.karololejarz.currencyaccount.dto.AccountCreateDto;
import com.karololejarz.currencyaccount.entity.Account;
import com.karololejarz.currencyaccount.entity.AccountType;
import com.karololejarz.currencyaccount.entity.Customer;
import com.karololejarz.currencyaccount.service.AccountService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.google.gson.Gson;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(AccountController.class)
class AccountControllerTests {
  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private AccountService accountService;

  @Test
  void shouldCreateAccountWith10Initial() throws Exception {
    AccountCreateDto createDto = new AccountCreateDto("First", "Last", BigDecimal.valueOf(40L));
    AccountType accountType = new AccountType(1L, "Rachunek Walutowy", "USD", BigDecimal.valueOf(4L), BigDecimal.valueOf(4L), BigDecimal.valueOf(4L), null);
    Customer owner = new Customer(createDto);
    Account account = new Account(accountType, owner, BigDecimal.TEN);

    when(accountService.openAccountWithStartingBalance(ArgumentMatchers.any(AccountCreateDto.class))).thenReturn(account);

    Gson gson = new Gson();
    String jsonContent = gson.toJson(createDto);

    mockMvc.perform(post("/api/accounts/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(jsonContent)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.firstName", is("First")))
        .andExpect(jsonPath("$.lastName", is("Last")))
        .andExpect(jsonPath("$.balance", is(10.0)));
  }
}
