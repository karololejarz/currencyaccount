package com.karololejarz.currencyaccount.dto;

import com.karololejarz.currencyaccount.entity.Operation;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

@Data
public class OperationDto {
  Long accountNumber;
  String operationName;
  BigDecimal operationValue;
  BigDecimal balanceAfter;
  String currency;
  BigDecimal balanceInPln;
  LocalDateTime time;

  public OperationDto(Operation operation) {
    this.accountNumber = operation.getAccount().getAccountNumber();
    this.operationName = operation.getOperationName();
    this.operationValue = operation.getOperationValue();
    this.balanceAfter = operation.getBalanceAfter().setScale(2, RoundingMode.FLOOR);
    this.currency = operation.getAccount().getAccountType().getCurrency();
    this.balanceInPln = operation.getBalanceAfter().multiply(operation.getAccount().getAccountType().getOfficialPrice()).setScale(2, RoundingMode.FLOOR);
    this.time = operation.getCreatedAt();
  }
}
