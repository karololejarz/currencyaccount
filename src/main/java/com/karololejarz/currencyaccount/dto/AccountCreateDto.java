package com.karololejarz.currencyaccount.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class AccountCreateDto {
  private String firstName;
  private String lastName;
  private BigDecimal balance;
}
