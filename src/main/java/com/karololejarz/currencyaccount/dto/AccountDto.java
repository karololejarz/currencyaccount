package com.karololejarz.currencyaccount.dto;

import com.karololejarz.currencyaccount.entity.Account;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
@AllArgsConstructor
public class AccountDto {
  private Long accountNumber;
  private String accountType;
  private String firstName;
  private String lastName;
  private BigDecimal balance;
  private String currency;
  private BigDecimal balanceInPln;

  public AccountDto(Account account) {
    this.accountNumber = account.getAccountNumber();
    this.accountType = account.getAccountType().getCommercialName();
    this.firstName = account.getOwner().getFirstName();
    this.lastName = account.getOwner().getLastName();
    this.balance = account.getBalance().setScale(2, RoundingMode.FLOOR);
    this.currency = account.getAccountType().getCurrency();
    this.balanceInPln = account.getBalance().multiply(account.getAccountType().getOfficialPrice()).setScale(2, RoundingMode.FLOOR);

  }
}
