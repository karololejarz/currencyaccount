package com.karololejarz.currencyaccount.operations.credit;

import com.karololejarz.currencyaccount.entity.Account;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class CreditAccountWithUSD implements CreditOperation {

  @Override
  public String getName() {
    return "CREDITED_WITH_USD";
  }

  @Override
  public Account creditAccount(CreditOperationContext coc) {
    Account account = coc.getAccount();
    account.setBalance(account.getBalance().add(coc.getAmount()));
    return account;
  }
}
