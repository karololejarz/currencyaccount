package com.karololejarz.currencyaccount.operations.credit;

import com.karololejarz.currencyaccount.entity.Account;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class CreditOperationContext {
  private Account account;
  private BigDecimal amount;
  private CreditOperation creditOperation;

  public Account creditAccount() {
    return creditOperation.creditAccount(new CreditOperationContext(account, amount, creditOperation));
  }
}
