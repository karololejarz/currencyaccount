package com.karololejarz.currencyaccount.operations.credit;

import com.karololejarz.currencyaccount.entity.Account;

public interface CreditOperation {
  public String getName();
  public Account creditAccount(CreditOperationContext coc);
}
