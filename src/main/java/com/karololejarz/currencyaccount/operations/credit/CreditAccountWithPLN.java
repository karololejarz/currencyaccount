package com.karololejarz.currencyaccount.operations.credit;

import com.karololejarz.currencyaccount.entity.Account;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class CreditAccountWithPLN implements CreditOperation {

  @Override
  public String getName() {
    return "CREDITED_WITH_PLN";
  }

  @Override
  public Account creditAccount(CreditOperationContext coc) {
    Account account = coc.getAccount();
    BigDecimal askPrice = coc.getAccount().getAccountType().getAskPrice();
    account.setBalance(account.getBalance().add(coc.getAmount().divide(askPrice, 2, RoundingMode.FLOOR)));
    return account;
  }
}
