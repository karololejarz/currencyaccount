package com.karololejarz.currencyaccount.operations.debit;

import com.karololejarz.currencyaccount.entity.Account;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class DebitOperationContext {
  private Account account;
  private BigDecimal amount;
  private DebitOperation debitOperation;

  public Account debitAccount() {
    return debitOperation.debitAccount(new DebitOperationContext(account, amount, debitOperation));
  }
}
