package com.karololejarz.currencyaccount.operations.debit;

import com.karololejarz.currencyaccount.entity.Account;

public interface DebitOperation {
  public String getName();
  public Account debitAccount (DebitOperationContext doc);
}
