package com.karololejarz.currencyaccount.operations.debit;

import com.karololejarz.currencyaccount.entity.Account;
import org.springframework.stereotype.Component;

@Component
public class WithdrawUSD implements DebitOperation{
  @Override
  public String getName() {
    return "WITHDRAW_USD";
  }

  @Override
  public Account debitAccount(DebitOperationContext doc) {
    Account account = doc.getAccount();
    account.setBalance(account.getBalance().subtract(doc.getAmount()));
    return account;
  }
}
