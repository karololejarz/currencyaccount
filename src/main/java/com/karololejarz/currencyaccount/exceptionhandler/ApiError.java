package com.karololejarz.currencyaccount.exceptionhandler;

import java.util.List;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiError {

  private HttpStatus status;
  private List<String> errors;

  public ApiError(final HttpStatus status, final List<String> errors) {
    this.status = status;
    this.errors = errors;
  }

}
