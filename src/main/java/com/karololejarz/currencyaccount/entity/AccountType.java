package com.karololejarz.currencyaccount.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "AccountTypes")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountType {
  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  private Long id;
  String commercialName;
  String currency;
  @Column(precision = 19, scale = 4)
  BigDecimal officialPrice;
  @Column(precision = 19, scale = 4)
  BigDecimal bidPrice;
  @Column(precision = 19, scale = 4)
  BigDecimal askPrice;
  BigDecimal interest;


  public AccountType(String commercialName, String currency, BigDecimal exchangeRate, BigDecimal spread, BigDecimal interest) {
    this.commercialName = commercialName;
    this.currency = currency;
    this.officialPrice = exchangeRate;
    this.bidPrice = exchangeRate.multiply(BigDecimal.ONE.subtract(spread.multiply(BigDecimal.valueOf(0.5))));
    this.askPrice = exchangeRate.multiply(BigDecimal.ONE.add(spread.multiply(BigDecimal.valueOf(0.5))));
    this.interest = interest;
  }
}
