package com.karololejarz.currencyaccount.entity;

import com.karololejarz.currencyaccount.officialrateresponse.OfficialRateResponseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = false)
@Entity(name = "OfficialRates")
@Data
@NoArgsConstructor
public class OfficialRate extends BaseEntity{

  private String code;
  private String exchangeTable;
  private LocalDate effectiveDate;
  @Column(precision = 19, scale = 4)
  private BigDecimal value;

  public OfficialRate(OfficialRateResponseDto dto) {
    this.code = dto.getCode();
    this.exchangeTable = dto.getOfficialRateDtos().get(0).getNo();
    this.effectiveDate = LocalDate.parse(dto.getOfficialRateDtos().get(0).getEffectiveDate());
    this.value = dto.getOfficialRateDtos().get(0).getMid().setScale(4, RoundingMode.HALF_DOWN);
    this.setCreatedAt(LocalDateTime.now());
    this.setModifiedAt(LocalDateTime.now());
  }

}
