package com.karololejarz.currencyaccount.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = false)
@Entity(name = "Operations")
@Data
@NoArgsConstructor
public class Operation extends BaseEntity{
  @ManyToOne
  @JoinColumn(name = "account_number", nullable = false)
  Account account;
  String operationName;
  @Column(precision = 19, scale = 2)
  @Positive(message = "Operation has to be with positive amount")
  @Digits(integer = 6, fraction = 2, message = "Operation has more than fractional 2 digits or has more than 6 integral digits")
  BigDecimal operationValue;
  @Column(precision = 19, scale = 2)
  BigDecimal balanceAfter;

  public Operation(Account account, String operationName, BigDecimal operationValue) {
    this.account = account;
    this.operationName = operationName;
    this.operationValue = operationValue;
    this.setCreatedAt(LocalDateTime.now());
    this.setModifiedAt(LocalDateTime.now());
    this.balanceAfter = account.getBalance();
  }
}
