package com.karololejarz.currencyaccount.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Entity(name = "Accounts")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account{
  @Id
  @GenericGenerator(name = "account_id", strategy = "com.karololejarz.currencyaccount.generator.AccountIdGenerator")
  @GeneratedValue(generator = "account_id")
  private Long accountNumber;
  private LocalDateTime createdAt;
  private LocalDateTime modifiedAt;
  @OneToOne(cascade = {CascadeType.ALL})
  private AccountType accountType;
  @OneToOne(cascade = {CascadeType.ALL})
  private Customer owner;
  private BigDecimal balance;
  @OneToMany(mappedBy="account", fetch = FetchType.LAZY)
  private List<Operation> operations;

  public Account(AccountType accountType, Customer customer, BigDecimal balance) {
    this.accountType = accountType;
    this.owner = customer;
    this.balance = balance;
    this.setCreatedAt(LocalDateTime.now());
    this.setModifiedAt(LocalDateTime.now());
  }
}
