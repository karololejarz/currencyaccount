package com.karololejarz.currencyaccount.entity;

import com.karololejarz.currencyaccount.dto.AccountCreateDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = false)
@Entity(name = "Customers")
@Data
@NoArgsConstructor
public class Customer extends BaseEntity{
  @Size(min = 1, max = 50, message = "First name has to be between 1 and 50 characters")
  private String firstName;
  @Size(min = 1, max = 50, message = "Last name has to be between 1 and 50 characters")
  private String lastName;

  public Customer(AccountCreateDto dto) {
    this.firstName = dto.getFirstName();
    this.lastName = dto.getLastName();
    this.setCreatedAt(LocalDateTime.now());
    this.setModifiedAt(LocalDateTime.now());
  }
}
