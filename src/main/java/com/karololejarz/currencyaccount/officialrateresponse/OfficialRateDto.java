package com.karololejarz.currencyaccount.officialrateresponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "no",
    "effectiveDate",
    "mid"
})
@Data
public class OfficialRateDto {
  @JsonProperty("no")
  private String no;
  @JsonProperty("effectiveDate")
  private String effectiveDate;
  @JsonProperty("mid")
  private BigDecimal mid;
}
