package com.karololejarz.currencyaccount.officialrateresponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "table",
    "currency",
    "code",
    "rates"
})
@Data
public class OfficialRateResponseDto {
  @JsonProperty("table")
  private String table;
  @JsonProperty("currency")
  private String currency;
  @JsonProperty("code")
  private String code;
  @JsonProperty("rates")
  private List<OfficialRateDto> officialRateDtos = null;
}


