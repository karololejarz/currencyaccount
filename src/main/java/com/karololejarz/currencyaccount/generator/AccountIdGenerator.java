package com.karololejarz.currencyaccount.generator;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AccountIdGenerator implements IdentifierGenerator {
  @Override
  public Serializable generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws HibernateException {
    Connection connection = sharedSessionContractImplementor.connection();
    try {
      Statement statement = connection.createStatement();
      ResultSet rs = statement.executeQuery("select count(account_number) as Id from Accounts");
      if(rs.next()) {
        return 123456789+rs.getLong(1);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public boolean supportsJdbcBatchInserts() {
    return false;
  }
}
