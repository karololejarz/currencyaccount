package com.karololejarz.currencyaccount.service;

import com.karololejarz.currencyaccount.entity.OfficialRate;
import com.karololejarz.currencyaccount.repository.OfficialRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfficialRateService {

  private final OfficialRateRepository officialRateRepository;

  @Autowired
  public OfficialRateService(OfficialRateRepository officialRateRepository) {
    this.officialRateRepository = officialRateRepository;
  }

  public OfficialRate getOfficialRate() {
    return officialRateRepository.findAll().iterator().next();
  }
}
