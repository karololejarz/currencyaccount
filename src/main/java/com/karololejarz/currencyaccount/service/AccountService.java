package com.karololejarz.currencyaccount.service;

import com.karololejarz.currencyaccount.entity.*;
import com.karololejarz.currencyaccount.dto.AccountCreateDto;
import com.karololejarz.currencyaccount.repository.AccountRepository;
import com.karololejarz.currencyaccount.repository.AccountTypeRepository;
import com.karololejarz.currencyaccount.repository.OfficialRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class AccountService {

  private final AccountRepository accountRepository;
  private final AccountTypeRepository accountTypeRepository;
  private final OfficialRateRepository officialRateRepository;
  private final OperationService operationService;

  @Autowired
  public AccountService(AccountRepository accountRepository, AccountTypeRepository accountTypeRepository, OfficialRateRepository officialRateRepository, OperationService operationService) {
    this.accountRepository = accountRepository;
    this.accountTypeRepository = accountTypeRepository;
    this.officialRateRepository = officialRateRepository;
    this.operationService = operationService;
  }

  public Account getById(Long id) {
    return accountRepository.getById(id);
  }

  public Account saveAccount(Account account) {
    return accountRepository.save(account);
  };

  public Account openAccountWithStartingBalance(AccountCreateDto dto) {

    OfficialRate officialRate = officialRateRepository.findById(1L).get();
    BigDecimal spread = BigDecimal.ZERO;

    AccountType accountType = new AccountType("Rachunek Walutowy", officialRate.getCode(), officialRate.getValue(), spread, BigDecimal.ZERO);
    if(accountTypeRepository.count() == 0) accountTypeRepository.save(accountType);
    accountType = accountTypeRepository.getByCommercialName("Rachunek Walutowy");
    BigDecimal usdInPln = dto.getBalance().divide(accountType.getAskPrice(), 2, RoundingMode.FLOOR);

    Account account = accountRepository.save(new Account(accountType, new Customer(dto), usdInPln));
    operationService.saveOperation(new Operation(account, "INITIAL_BALANCE", usdInPln));
    return account;
  }

  public Account getAccountByAccountNumber(Long accountNumber) {
    return accountRepository.getById(accountNumber);
  }
}
