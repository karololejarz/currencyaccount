package com.karololejarz.currencyaccount.service;

import com.karololejarz.currencyaccount.entity.Account;
import com.karololejarz.currencyaccount.entity.Operation;
import com.karololejarz.currencyaccount.operations.credit.CreditAccountWithPLN;
import com.karololejarz.currencyaccount.operations.credit.CreditAccountWithUSD;
import com.karololejarz.currencyaccount.operations.credit.CreditOperationContext;
import com.karololejarz.currencyaccount.operations.debit.CardTransactionUSD;
import com.karololejarz.currencyaccount.operations.debit.DebitOperationContext;
import com.karololejarz.currencyaccount.operations.debit.WithdrawUSD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class DebitOperationService {

  private final AccountService accountService;
  private final OperationService operationService;
  private final CardTransactionUSD cardTransactionUSD;
  private final WithdrawUSD withdrawUSD;

  @Autowired
  public DebitOperationService(AccountService accountService, CardTransactionUSD cardTransactionUSD, WithdrawUSD withdrawUSD, OperationService operationService) {
    this.cardTransactionUSD = cardTransactionUSD;
    this.withdrawUSD = withdrawUSD;
    this.accountService = accountService;
    this.operationService = operationService;
  }

  public Operation payWithCardUSD(Long id, BigDecimal amount) {
    CardTransactionUSD op = new CardTransactionUSD();
    Account account = accountService.saveAccount(cardTransactionUSD.debitAccount(new DebitOperationContext(accountService.getById(id), amount, new CardTransactionUSD())));
    return operationService.saveOperation(new Operation(account, op.getName(), amount));
  }

  public Operation withdrawUSD(Long id, BigDecimal amount) {
    WithdrawUSD op = new WithdrawUSD();
    Account account = accountService.saveAccount(withdrawUSD.debitAccount(new DebitOperationContext(accountService.getById(id), amount, new WithdrawUSD())));
    return operationService.saveOperation(new Operation(account, op.getName(), amount));
  }

}
