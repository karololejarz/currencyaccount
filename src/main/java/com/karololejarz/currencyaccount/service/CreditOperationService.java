package com.karololejarz.currencyaccount.service;

import com.karololejarz.currencyaccount.entity.Account;
import com.karololejarz.currencyaccount.entity.Operation;
import com.karololejarz.currencyaccount.operations.credit.CreditAccountWithPLN;
import com.karololejarz.currencyaccount.operations.credit.CreditAccountWithUSD;
import com.karololejarz.currencyaccount.operations.credit.CreditOperationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CreditOperationService {

  private final AccountService accountService;
  private final OperationService operationService;
  private final CreditAccountWithUSD creditAccountWithUSD;
  private final CreditAccountWithPLN creditAccountWithPLN;

  @Autowired
  public CreditOperationService(CreditAccountWithUSD creditAccountWithUSD, CreditAccountWithPLN creditAccountWithPLN, AccountService accountService, OperationService operationService) {
    this.creditAccountWithUSD = creditAccountWithUSD;
    this.creditAccountWithPLN = creditAccountWithPLN;
    this.accountService = accountService;
    this.operationService = operationService;
  }

  public Operation creditAccountWithUSD(Long id, BigDecimal amount) {
    CreditAccountWithUSD op = new CreditAccountWithUSD();
    Account account = accountService.saveAccount(creditAccountWithUSD.creditAccount(new CreditOperationContext(accountService.getById(id), amount, op)));
    return operationService.saveOperation(new Operation(account, op.getName(), amount));
  }

  public Operation creditAccountWithPLN(Long id, BigDecimal amount) {
    CreditAccountWithPLN op = new CreditAccountWithPLN();
    Account account = accountService.saveAccount(creditAccountWithPLN.creditAccount(new CreditOperationContext(accountService.getById(id), amount, new CreditAccountWithPLN())));
    return operationService.saveOperation(new Operation(account, op.getName(), amount));
  }
}
