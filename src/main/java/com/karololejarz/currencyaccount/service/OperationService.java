package com.karololejarz.currencyaccount.service;

import com.karololejarz.currencyaccount.entity.Operation;
import com.karololejarz.currencyaccount.repository.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperationService {

  private final OperationRepository operationRepository;

  @Autowired
  public OperationService(OperationRepository operationRepository) {
    this.operationRepository = operationRepository;
  }

  public Operation saveOperation(Operation operation) {
    return operationRepository.save(operation);
  }

  public List<Operation> getOperationsByAccountId(Long accountId) {
    return operationRepository.getAllByAccountId(accountId);
  }
}
