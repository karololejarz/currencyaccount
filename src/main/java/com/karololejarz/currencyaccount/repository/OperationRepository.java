package com.karololejarz.currencyaccount.repository;

import com.karololejarz.currencyaccount.entity.Operation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {
  @Query("select o from Operations o where o.account.id=:accountId")
  List<Operation> getAllByAccountId(Long accountId);
}
