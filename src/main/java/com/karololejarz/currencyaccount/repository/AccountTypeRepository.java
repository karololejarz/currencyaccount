package com.karololejarz.currencyaccount.repository;

import com.karololejarz.currencyaccount.entity.AccountType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountTypeRepository extends JpaRepository<AccountType, Long> {

  AccountType getByCommercialName(String commercialName);
}
