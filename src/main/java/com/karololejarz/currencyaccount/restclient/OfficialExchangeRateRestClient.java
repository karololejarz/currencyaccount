package com.karololejarz.currencyaccount.restclient;

import com.karololejarz.currencyaccount.entity.OfficialRate;
import com.karololejarz.currencyaccount.officialrateresponse.OfficialRateResponseDto;
import com.karololejarz.currencyaccount.repository.OfficialRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.URISyntaxException;

@Component
public class OfficialExchangeRateRestClient {
  @Value("${nbp.rates}")
  String ratesUrl;

  RestTemplate restTemplate;
  OfficialRateRepository officialRateRepository;

  @Autowired
  public OfficialExchangeRateRestClient(RestTemplateBuilder restTemplateBuilder, OfficialRateRepository officialRateRepository) {
    this.restTemplate = restTemplateBuilder.build();
    this.officialRateRepository = officialRateRepository;
  }

  @PostConstruct
  public OfficialRateResponseDto getExchangeRateResponseAndSaveIt() {
    OfficialRateResponseDto result = null;
    try {
      URI uri = new URI(ratesUrl);
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_JSON);
      HttpEntity<String> request = new HttpEntity<>(headers);
      ResponseEntity<OfficialRateResponseDto> responseEntity =
          restTemplate.exchange(uri + "A/USD?format=json", HttpMethod.GET, request, OfficialRateResponseDto.class);
      result = responseEntity.getBody();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
    System.out.println(result);
    assert result != null;
    officialRateRepository.save(new OfficialRate(result));
    return result;
  }

}
