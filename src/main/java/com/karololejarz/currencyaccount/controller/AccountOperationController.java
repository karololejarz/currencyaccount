package com.karololejarz.currencyaccount.controller;

import com.karololejarz.currencyaccount.dto.OperationDto;
import com.karololejarz.currencyaccount.service.CreditOperationService;
import com.karololejarz.currencyaccount.service.DebitOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/accounts")
public class AccountOperationController {

  CreditOperationService creditOperationService;
  DebitOperationService debitOperationService;

  @Autowired
  public AccountOperationController(CreditOperationService creditOperationService, DebitOperationService debitOperationService) {
    this.creditOperationService = creditOperationService;
    this.debitOperationService = debitOperationService;
  }

  @PostMapping(value = {"/{accountNumber}/creditWithUSD"})
  public OperationDto creditAccountWithUSD(@PathVariable("accountNumber") Long id, BigDecimal amount) {
    return new OperationDto(creditOperationService.creditAccountWithUSD(id, amount));
  }

  @PostMapping(value = {"/{accountNumber}/creditWithPLN"})
  public OperationDto creditAccountWithPLN(@PathVariable("accountNumber") Long id, BigDecimal amount) {
    return new OperationDto(creditOperationService.creditAccountWithPLN(id, amount));
  }

  @PostMapping(value = {"/{accountNumber}/payWithCardUSD"})
  public OperationDto payWithCardUSD(@PathVariable("accountNumber") Long id, BigDecimal amount) {
    return new OperationDto(debitOperationService.payWithCardUSD(id, amount));
  }

  @PostMapping(value = {"/{accountNumber}/withdrawUSD"})
  public OperationDto withdrawUSD(@PathVariable("accountNumber") Long id, BigDecimal amount) {
    return new OperationDto(debitOperationService.withdrawUSD(id, amount));
  }
}
