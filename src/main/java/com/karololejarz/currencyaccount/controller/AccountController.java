package com.karololejarz.currencyaccount.controller;

import com.karololejarz.currencyaccount.dto.AccountDto;
import com.karololejarz.currencyaccount.dto.AccountCreateDto;
import com.karololejarz.currencyaccount.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/accounts/")
public class AccountController {

  AccountService accountService;

  @Autowired
  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  @PostMapping(consumes = APPLICATION_JSON_VALUE)
  public AccountDto openAccountWithStartingBalance(@RequestBody AccountCreateDto dto) {
    return new AccountDto(accountService.openAccountWithStartingBalance(dto));
  }

  @GetMapping("{accountNumber}")
  public AccountDto getAccount(@PathVariable("accountNumber") Long accountNumber) {
    return new AccountDto(accountService.getAccountByAccountNumber(accountNumber));
  }
}
