package com.karololejarz.currencyaccount.controller;

import com.karololejarz.currencyaccount.dto.OperationDto;
import com.karololejarz.currencyaccount.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/operations/")
public class OperationController {

  private final OperationService operationService;

  @Autowired
  public OperationController(OperationService operationService) {
    this.operationService = operationService;
  }

  @GetMapping("/accounts/{accountNumber}")
  public List<OperationDto> getOperationsByAccountNumber(@PathVariable("accountNumber") Long accountId) {
    return operationService.getOperationsByAccountId(accountId).stream().map(OperationDto::new).collect(Collectors.toList());
  }
}
